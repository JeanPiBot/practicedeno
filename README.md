# Learning Deno and Typescript

Samples for practice Deno and Typescript


# Run

If you just want to run, you can use a command which is:

```
deno run [name].ts
```
Example:

```
deno run index.ts
```


# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


# Authors
* **Jean Pierre Giovanni Arenas Ortiz**

# License
[MIT](https://choosealicense.com/licenses/mit/)
