

const miArray: number[] = [1,3,2,4,10,23,202,384];

const binarySearch = (array: number[], item: number) => {
    let min = 0;
    let max = array.length -1;

    while (min <= max) {
        let middle = Math.floor((min + max) / 2);
        let guess = array[middle];

        if ( guess === item) {
            return middle;
        }

        if ( guess < item) {
            min = middle + 1;
        } else {
            max = middle - 1;
        }
    }

    return -1;

}

console.log(binarySearch(miArray, 10));
console.log(binarySearch(miArray, 4));
console.log(binarySearch(miArray, 202));
console.log(binarySearch(miArray, 1));